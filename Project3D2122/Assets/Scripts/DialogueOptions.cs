using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DialogueOptions : MonoBehaviour
{
    public AudioClip A0;
    public AudioClip A1_1;
    public AudioClip A1_2;
    public AudioClip A1_3;
    public AudioClip A2_1;
    public AudioClip A2_2;
    public AudioClip A2_3;
    public AudioClip A3_1;
    public AudioClip A3_2;
    public AudioClip A3_3;
    public AudioClip A4_1;
    public AudioClip A4_2;
    public AudioClip A4_3;
    public AudioClip A5_1;
    public AudioClip A5_2;
    public AudioClip A5_3;
    public AudioClip A6_1;
    public AudioClip A6_2;
    public AudioClip A6_3;
    public AudioClip A7_1;
    public AudioClip A7_2;
    public AudioClip A7_3;
    public List<AudioClip> audioclips;

    public List<string> AggroDialogue(int situation, int aggroLevel)
    {
        List<string> sentences = new List<string>();
        audioclips = new List<AudioClip>();
        switch (situation) //for all the aggro situations
        {
            case 0: 
                if (aggroLevel == 0)
                {
                    audioclips.Add(A0);
                    sentences.Add("*Zucht* Als het mij maar snel beter maakt, dan.");
                } //if the aggrolevel of the patient is 0, these sentences can be spoken by the patient
                else if (aggroLevel == 1) //patient is annoyed, and squeezes his hands into fists
                {
                    audioclips.Add(A1_1);
                    audioclips.Add(A1_2);
                    audioclips.Add(A1_3);
                    sentences.Add("Nee.Ik wil dat spul niet nemen.Het is vies, en lijkt mij heel ongezond.");
                    sentences.Add("Hoezo moet het? Ik heb het helemaal niet nodig, ik genees wel.");
                    sentences.Add("Ik ben zo vaak ziek geweest, en ik ben nu toch ook beter?");
                }
                else if (aggroLevel == 2) //patient is restless, and paces
                {
                    audioclips.Add(A2_1);
                    audioclips.Add(A2_2);
                    audioclips.Add(A2_3);
                    sentences.Add("Onzin! Allemaal onzin. Ik heb het overleefd door bloed en zweet, niet door hocus pocus!");
                    sentences.Add("Magische drankjes zijn voor de zwakken, ik weiger het te nemen!");
                    sentences.Add("Abracadabra! Kijk, helemaal genezen. Geen spul voor nodig.");
                }
                else if (aggroLevel == 3) //patient stands within the comfort zone of the doctor, and shouts
                {
                    audioclips.Add(A3_1);
                    audioclips.Add(A3_2);
                    audioclips.Add(A3_3);
                    sentences.Add("Wilt u mij vergiftigen of zo, met dat vreemde goedje?!");
                    sentences.Add("Voor al ik weet zit daar de pest in!");
                    sentences.Add("Heeft u geen normale kuren, zoals sterke mede of zo? Kwakzalver!");
                }
                else if (aggroLevel == 4) //patient threatens to throw an item in the room
                {
                    audioclips.Add(A4_1);
                    audioclips.Add(A4_2);
                    audioclips.Add(A4_3);
                    sentences.Add("Zeg, ga toch eens weg met je \"medicijn\"!");
                    sentences.Add("Rot op met dat vuile goedje!");
                    sentences.Add("Scheer je weg met je walgelijke drankjes!");
                }
                else if (aggroLevel == 5) //patient threatens to throw an item at the doctor
                {
                    audioclips.Add(A5_1);
                    audioclips.Add(A5_2);
                    audioclips.Add(A5_3);
                    sentences.Add("Vlieg op met je kikkerogen en rattenpuisten!");
                    sentences.Add("Je bent hiet niet welkom, ga weg!");
                    sentences.Add("Wees wijs, rot op!");
                }
                else if (aggroLevel == 6) //patient threatens physical violence 
                {
                    audioclips.Add(A6_1);
                    audioclips.Add(A6_2);
                    audioclips.Add(A6_3);
                    sentences.Add("Ga weg, of ik MAAK u weg gaan!");
                    sentences.Add("Moet je die \"medicatie\" naar je hoofd hebben of zo?!");
                    sentences.Add("Ik zal je eens een goed remedie laten VOELEN!");
                }
                else if (aggroLevel == 7) //patient goes over to physical violence
                {
                    audioclips.Add(A7_1);
                    audioclips.Add(A7_2);
                    audioclips.Add(A7_3);
                    sentences.Add("Malloot!");
                    sentences.Add("Vuile magier!");
                    sentences.Add("Hondsdol mormel!");
                }
                break;
            case 1: //patient wants to go outside but isn't allowed to
                if (aggroLevel == 0)
                {
                    audioclips.Add(A0);
                    sentences.Add("Okee! Okee! U zal wel gelijk hebben. Laat mij maar van verveling sterven, en treiter iemand anders.");
                }
                else if (aggroLevel == 1)
                {
                    audioclips.Add(A1_1);
                    audioclips.Add(A1_2);
                    audioclips.Add(A1_3);
                    sentences.Add("Hee, hallo! Ik wil naar buiten! Het is hier maar saai!");
                    sentences.Add("Laat me gewoon gaan, zeg.");
                    sentences.Add("Maar een briesje is goed voor de ziel!");
                }
                else if (aggroLevel == 2)
                {
                    audioclips.Add(A2_1);
                    audioclips.Add(A2_2);
                    audioclips.Add(A2_3);
                    sentences.Add("U kan dat allemaal wel vinden, ja, maar wat van mij?");
                    sentences.Add("Wat boeit het dat ik ziek ben? Het is gewoon een verkoudheid!");
                    sentences.Add("Ik wil gewoon even naar buiten, ben ik hier een gevangene of zo?");
                }
                else if (aggroLevel == 3)
                {
                    audioclips.Add(A3_1);
                    audioclips.Add(A3_2);
                    audioclips.Add(A3_3);
                    sentences.Add("Jij geeft alleen om het geld dat je hiervoor krijgt, wat boeit het als ik naar buiten ga?");
                    sentences.Add("Asociaal hoor. Ik ben ziek, en je maakt me alleen zieker, zo!");
                    sentences.Add("Zo zie je maar weer waar de maatschappij naartoe gaat!");
                }
                else if (aggroLevel == 4)
                {
                    audioclips.Add(A4_1);
                    audioclips.Add(A4_2);
                    audioclips.Add(A4_3);
                    sentences.Add("Ga weg, dan doe ik m'n eigen ding!");
                    sentences.Add("Hoe kan ik je missen, als je niet oprot?");
                    sentences.Add("Het leven zit vol teleurstellingen, en jij bent er een van!");
                }
                else if (aggroLevel == 5)
                {
                    audioclips.Add(A5_1);
                    audioclips.Add(A5_2);
                    audioclips.Add(A5_3);
                    sentences.Add("Krijg toch de klere!");
                    sentences.Add("Scheer je weg, duivelsgebroed!");
                    sentences.Add("Laat me gaan, serpent!");
                }
                else if (aggroLevel == 6) //threatens physical violence
                {
                    audioclips.Add(A6_1);
                    audioclips.Add(A6_2);
                    audioclips.Add(A6_3);
                    sentences.Add("Zeg naarling, ga toch een graf zoeken!");
                    sentences.Add("Vlieg op, jij achterlijke gladiool.");
                    sentences.Add("Ik maak je af, druiloor!");
                }
                else if (aggroLevel == 7)
                {
                    audioclips.Add(A7_1);
                    audioclips.Add(A7_2);
                    sentences.Add("Opperprutser!");
                    sentences.Add("Vlegel!");
                    //sentences.Add("Snertjong!");
                }
                break;
            case 2: //patient is demented 
                if (aggroLevel == 0)
                {
                    sentences.Add("Ik...ik zal het vergeten zijn. Ga maar. Ik ga...slapen, denk ik.");
                    sentences.Add("...");
                    sentences.Add("...");
                }
                else if (aggroLevel == 1)
                {
                    sentences.Add("Ik weet zeker dat dit mijn huis niet is.");
                    sentences.Add("Kom aan zeg, u wilt toch niet zeggen dat ik niet weet wat thuis is?");
                    sentences.Add("Nee? Echt? Da's toch pure onzin?");
                }
                else if (aggroLevel == 2)
                {
                    sentences.Add("Weet u dat zeker? Wat als u mij voorliegt?");
                    sentences.Add("Jij bent te jong om er iets van te snappen, zie ik!");
                    sentences.Add("Mag ik nu eindelijk naar huis?");
                }
                else if (aggroLevel == 3)
                {
                    sentences.Add("Waar ben ik? Ik wil naar huis!");
                    sentences.Add("Ik ken deze vreemde mensen niet! ");
                    sentences.Add("Ik wil mijn moeder!");
                }
                else if (aggroLevel == 4)
                {
                    sentences.Add("Laat me met rust! Ik wil weg!");
                    sentences.Add("U begrijpt het niet! Ik moet naar huis!");
                    sentences.Add("U zegt gewoon wat!");
                }
                else if (aggroLevel == 5)
                {
                    sentences.Add("Zomaar oude mensen treiteren! Ik zal je!");
                    sentences.Add("Stik toch in een mesthoop.");
                    sentences.Add("Laat me eruit!");
                }
                else if (aggroLevel == 6)
                {
                    sentences.Add("Kom hier, dat ik uw ogen blauw sla!");
                    sentences.Add("Ren weg, lomperik, voor ik mijn geduld verlies!");
                    sentences.Add("Wat moet je, een klap?");
                    
                } //threatens physical violence
                else if (aggroLevel == 7)
                {
                    sentences.Add("Dom geboefte!");
                    sentences.Add("Vervloekt gespuis!");
                    sentences.Add("Helleveeg!");
                }
                break;
            case 3: //patient is very annoyed by the music of the market
                if (aggroLevel == 0)
                {
                    sentences.Add("U heeft gelijk. Ik zal de luiken maar dicht doen, dan hoor ik ze minder.");
                    sentences.Add("...");
                    sentences.Add("...");
                }
                else if (aggroLevel == 1)
                {
                    sentences.Add("Mensen! Mag dat wat stiller? Ik wil slapen!");
                    sentences.Add("Kunnen jullie niet gaan spelen onder iemand anders z'n raam?");
                    sentences.Add("Ik wordt hier echt alleen nog zieker van. Bah.");
                }
                else if (aggroLevel == 2)
                {
                    sentences.Add("Wilt u hen naar rede doen luisteren?");
                    sentences.Add("Zij zijn gewoon super asociaal! En iedere dinsdag, ook!");
                    sentences.Add("Waarom rekening houden met je medemens?");
                }
                else if (aggroLevel == 3)
                {
                    sentences.Add("Jaag ze weg of zo, maar laat ze ophouden!");
                    sentences.Add("Alstublieft! Ik wordt hier gek van!");
                    sentences.Add("Waarom kunnen ze niet gewoon ophouden?!");
                }
                else if (aggroLevel == 4)
                {
                    sentences.Add("Zeg eens, oelewapper! Doe er wat aan!");
                    sentences.Add("Breek toch hun instrumenten. Of hun benen! Boeit me niet!");
                    sentences.Add("Ik wordt hier doof, potverdorie!");
                }
                else if (aggroLevel == 5)
                {
                    sentences.Add("Naarlingen! Rot op met je kleremuziek!");
                    sentences.Add("Moeten ze een bedpan naar hun hoofd of zo?!");
                    sentences.Add("Wie ben jij?! Scheer je weg!");
                }
                else if (aggroLevel == 6) //threatens physical violence
                {
                    sentences.Add("Moet JIJ een bedpan naar je hoofd hebben of zo?");
                    sentences.Add("Verdwijn toch, zoals die andere kniesoren!");
                    sentences.Add("Zeg dwaas, moet ik je een klap verkopen?");
                }
                else if (aggroLevel == 7)
                {
                    sentences.Add("Domoor, je had weg moeten gaan toen het nog kon!");
                    sentences.Add("Pak aan!");
                    sentences.Add("Jankebal! Heikneuter!");
                }
                break;
            case 4: //patient is hungry, but there is hardly any food
                if (aggroLevel == 0)
                {
                    sentences.Add("Iets is beter dan niets, zeker. Ik zal het wel eten.");
                    sentences.Add("...");
                    sentences.Add("...");
                }
                else if (aggroLevel == 1)
                {
                    sentences.Add("Is er echt niets beters? Dit is nauwelijks soep te noemen!");
                    sentences.Add("Een boterham? Rijstebrei? Een stuk groente?!");
                    sentences.Add("Waterige soep...wat overkomt de wereld!");
                }
                else if (aggroLevel == 2)
                {
                    sentences.Add("Wat is dit voor waterig goedje? Dit is geen eten.");
                    sentences.Add("Hoezo, dit is het enigste dat er in de stad is?!");
                    sentences.Add("Wilt u mij van de honger laten sterven of zo?!");
                }
                else if (aggroLevel == 3)
                {
                    sentences.Add("Ja ja! U heeft het brood voor uzelf gehouden, zeker?!");
                    sentences.Add("Dief! Stelen van hulpelose mensen, wat denk je wel?!");
                    sentences.Add("Geef me gewoon wat brood of zo!");
                }
                else if (aggroLevel == 4)
                {
                    sentences.Add("Wie moet ik dan vragen voor een deftige maaltijd?!");
                    sentences.Add("Is het zo moelijk, een brood gaan halen?! Sneu hoor.");
                    sentences.Add("Vind u mijn lijden leuk, of zo?");
                }
                else if (aggroLevel == 5)
                {
                    sentences.Add("Ga gewoon iets normaals halen!");
                    sentences.Add("Verzuip toch in die \"soep\" van je!");
                    sentences.Add("Ga een brug zoeken of zo!");
                }
                else if (aggroLevel == 6)
                {
                    sentences.Add("Kijk, je haalt wat eten, of het zal je spijten!");
                    sentences.Add("Nu heb ik honger. U wilt niet zien wanneer ik kwaad ben!");
                    sentences.Add("Godverdomme, geef me gewoon normaal eten!");
                }
                else if (aggroLevel == 7)
                {
                    sentences.Add("Gulzigaard!");
                    sentences.Add("Dief!");
                    sentences.Add("Schandvlek voor de maatschappij!");
                }
                break;
            case 5: //family is mad at the doctor
                if (aggroLevel == 0)
                {
                    sentences.Add("We zien wel. Doe nu gewoon maar uw ding, en maak haar beter.");
                    sentences.Add("...");
                    sentences.Add("...");
                }
                else if (aggroLevel == 1)
                {
                    sentences.Add("Ik wil gewoon dat mijn moeder beter wordt!");
                    sentences.Add("Maak haar beter, en ik hou mijn mond.");
                    sentences.Add("Wat als ze niet geneest? Dan ligt het aan u!");
                }
                else if (aggroLevel == 2)
                {
                    sentences.Add("Nutteloos! Je bent gewoon volledig nutteloos!");
                    sentences.Add("Leugenaar. Zomaar een oude dame van haar geld ontroven!");
                    sentences.Add("Jij bent echt het onderkruipsel van geneesheren, zeker?");
                }
                else if (aggroLevel == 3)
                {
                    sentences.Add("Allemaal excuses om gewoon uw eigen ding te doen!");
                    sentences.Add("Moeder ligt hier al een maand, en wat doet u? Niets!");
                    sentences.Add("U geeft nergens om! Lekkere \"hulp\" is dit!");
                }
                else if (aggroLevel == 4)
                {
                    sentences.Add("Wat een grap van een behandeling is dit! U bent hier de helft van de tijd niet eens!");
                    sentences.Add("Stabiel? Stabiel is niet gezond, en daar bent u voor, toch?!");
                    sentences.Add("U geeft meer om u eigen dan om mijn doodzieke moeder!");
                }
                else if (aggroLevel == 5)
                {
                    sentences.Add("Een zieke grap, dat is het! Een zieke grap!");
                    sentences.Add("Als je niet helpen gaat, vlieg dan op!");
                    sentences.Add("Scheer je weg, pestjong!");
                }
                else if (aggroLevel == 6)
                {
                    sentences.Add("Zult u eens zien hoe het voelt om door u behandelt te worden!");
                    sentences.Add("Moet je een afspraak met mijn zwaard of zo?");
                    sentences.Add("Krijg ik jou doodziek in een bed!");
                }
                else if (aggroLevel == 7)
                {
                    sentences.Add("Schandknaap!");
                    sentences.Add("Kwakzalver!");
                    sentences.Add("Schurk!");
                }
                break;
            case 6: //family is mad at the doctor, as their relative died
                if (aggroLevel == 0)
                {
                    sentences.Add("Laat...laat me dit verwerken.");
                    sentences.Add("---");
                    sentences.Add("***");
                }
                else if (aggroLevel == 1)
                {
                    sentences.Add("Het leven is gewoon niet eerlijk.");
                    sentences.Add("Waarom moet mij dit nu overkomen? Waarom?!");
                    sentences.Add("Het is overduidelijk dat de wereld echt niets om ons mensen geeft.");
                }
                else if (aggroLevel == 2)
                {
                    sentences.Add("Ik zal hem nooit meer zien of vasthouden, en dat ligt aan u!");
                    sentences.Add("Hoe moet ik dit nu uitleggen? Hij ging genezen!");
                    sentences.Add("Wat moet ik nu?!");
                }
                else if (aggroLevel == 3)
                {
                    sentences.Add("Jij geeft er ook echt helemaal niets om!");
                    sentences.Add("Het is jou schuld dat hij dood is. Dood!");
                    sentences.Add("Ze komt nooit meer terug...nooit meer!");
                }
                else if (aggroLevel == 4)
                {
                    sentences.Add("Even zitten. Alsof dat helpen gaat!");
                    sentences.Add("Waarom heeft het leven mij dit aangedaan?");
                    sentences.Add("Breng hem terug! Breng mijn zoon terug!");
                }
                else if (aggroLevel == 5)
                {
                    sentences.Add("U zei dat hij beter zou worden! Hij is dood nu, dood!");
                    sentences.Add("Een beetje kiespijn, jij vuillak!");
                    sentences.Add("Ik hoop dat je hele familie \"kiespijn\" krijgt, schooier!");
                }
                else if (aggroLevel == 6)
                {
                    sentences.Add("Ik maak je af, zoals jij mijn zoon hebt afgemaakt!");
                    sentences.Add("Duik toch van een brug af!");
                    sentences.Add("Ik draai je nek om, en voer je aan de honden!");
                }
                else if (aggroLevel == 7)
                {
                    sentences.Add("Moordenaar!");
                    sentences.Add("Onmens!");
                    sentences.Add("Duivelsgebroed!");
                }
                break;
            case 7: //patient was wounded in battle: tis but a scratch
                if (aggroLevel == 0)
                {
                    sentences.Add("Is het er echt af? Echt? Wat vreemd. Verzorg het dan maar.");
                    sentences.Add("...");
                    sentences.Add("...");
                }
                else if (aggroLevel == 1)
                {
                    sentences.Add("Maar ik voel het! Hoe is mijn been dan weg?");
                    sentences.Add("Zie! Ik genees wel!");
                    sentences.Add("Ik heb het tot nu toch ook overleefd?");
                    
                }
                else if (aggroLevel == 2)
                {
                    sentences.Add("Ach nee, het is maar een krasje!");
                    sentences.Add("Maar een vleeswonde, dit verband is er allemaal niet voor nodig!");
                    sentences.Add("Ik voel me prima! Sterker dan ooit!");
                }
                else if (aggroLevel == 3)
                {
                    sentences.Add("Kijk een aan! Eindelijk iemand! Dat heeft lang genoeg geduurt!");
                    sentences.Add("Wat voor een kwakzalver ben jij?");
                    sentences.Add("Bla bla bla, wat voor gezever is dat?!");
                }
                else if (aggroLevel == 4)
                {
                    sentences.Add("Hoezo moet ik blijven liggen? Ik voel me prima!");
                    sentences.Add("Iedereen weet dat dat niet werkt, domoor.");
                    sentences.Add("Zeg hoor eens, ik zou het wel weten als ik neergestoken was!");
                }
                else if (aggroLevel == 5)
                {
                    sentences.Add("Moet ik boos gaan worden of zo?");
                    sentences.Add("U zult er spijt van hebben dat u Ridder Eustasius ooit dwarslag!");
                    sentences.Add("Ik weet het zelf beter dan jij, blaaskaak!");
                }
                else if (aggroLevel == 6)
                {
                    sentences.Add("Als ik hieruit kom, ga je d'r aan!");
                    sentences.Add("Boef! Ik wurg je!");
                    sentences.Add("Vlucht, voor je in mijn handen komt!");
                }
                else if (aggroLevel == 7)
                {
                    sentences.Add("Kom hier, dat ik u bijt!");
                    sentences.Add("Angsthaas! Stap dichter, dat ik u kopstoot!");
                    sentences.Add("Lastpak! Je durft gewoon niet!");
                }
                break;
        }
        return sentences;
    }

    public void PeaceDialogue(int situation)
    {
        List<string> sentences = new List<string>();
        if (situation == 0)
        {
            sentences[0] = "Hallo, wilt u mijn fles aangeven?";
            sentences[1] = "De fles water, alstublieft.";
            sentences[2] = "Geef het nou eens, ik heb dorst.";
        }
        else if (situation == 1)
        {
            sentences[0] = "Wilt u de luiken dicht doen?";
            sentences[1] = "Het is te licht, het geeft mij hoofdpijn.";
            sentences[2] = "Ik wordt hier verblind, due het dicht alsjeblieft!";
        }
        else if (situation == 2)
        {
            sentences[0] = "Mag ik mijn medicijnen?";
            sentences[1] = "Ik voel mij niet zo goed.";
            sentences[2] = "Medicatie, alstublieft.";
        }
        else if (situation == 3)
        {
            sentences[0] = "Wilt u mijn boek voorlezen? Ik ben blind, weet u.";
            sentences[1] = "...ben je naar buiten geslopen of zo?";
            sentences[2] = "Altublieft? ��n verhaaltje maar?";
        }
        else if (situation == 4)
        {
            sentences[0] = "MAG IK EEN NIEUW KUSSEN HEBBEN?";
            sentences[1] = "WATTE?! PRAAT NIET ZO ZACHTJES!";
            sentences[2] = "HOOR JE ME NIET? IK WIL EEN NIEUW KUSSEN!";
        }
        else if (situation == 5)
        {
            sentences[0] = "Wil je mijn knikkers aangeven?";
            sentences[1] = "Ik heb ze nog niet allemaal!";
            sentences[2] = "Toe, pak ze, alstublieft!";
        }
        else if (situation == 6)
        {
            sentences[0] = "Kunt u het raam open doen?";
            sentences[1] = "Het is zo muf hier!";
            sentences[2] = "Zucht. Ik zal wel van lucht tekort sterven.";
        }
        else if (situation == 7)
        {
            sentences[0] = "Een spin! Jaag het weg!";
            sentences[1] = "Alstublieft! Alstublieft! Red mij!";
            sentences[2] = "Aaaah! Ze zijn er nog! Doe iets!";
        }

    }
}
