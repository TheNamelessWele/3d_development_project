using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class SceneChange : MonoBehaviour
{
    public GameObject player;
    public GameObject spawnPoint;
    public int sceneNum; //number of the scene that'll be opened
    public Disinfect disinfect; //referencing other scripts
    public int levelIndex;
    public GameObject loadingScreen;
    public Slider slider;
    public Image fadescreen;    //image used to fade to black
    public Material fadeMat;

    public void MainSceneStart() //needs to be changed: depending on the door, a different scene should be loaded
    {
        /*if(isClean == true)
        {*/
        //SceneManager.LoadScene("Scene1");
        if (disinfect.isDisinfected)
        {
            StartCoroutine(DoorTransition("Z.Eikenhuis"));
            disinfect.isDisinfected = false;
        }
        else
        {
            Debug.LogError("Disinfect your hands first!");
        }
    }

    public void LoadLevel(int levelIndex)
    {
        Scene currentScene = SceneManager.GetActiveScene();
        if(currentScene.buildIndex == 0)
        {
            StartCoroutine(LoadAsync(levelIndex));
        }
        else
        {
            /*if (disinfect.isDisinfected)
            { 
                StartCoroutine(LoadAsync(levelIndex));
                disinfect.isDisinfected = false;
            }
            else
            {
                Debug.LogError("Wash your hands first!");
            }*/
            StartCoroutine(LoadAsync(levelIndex));
        }
        
    }

    public void BackToStreet()
    {
        //if(isClean == true)
        //{
        //SceneManager.LoadScene("Street");
        StartCoroutine(DoorTransition("Street"));
        //}
        /*else
        {
            Debug.Log("Wash your hands first!");
        }*/
        //for later: spawn in the same location as you left
    }

    public void DeathSpawn()
    {
        AsyncOperation operation = SceneManager.LoadSceneAsync("Street");
        player.transform.position = spawnPoint.transform.position;
        player.transform.rotation = spawnPoint.transform.rotation;
    }

    public void ExitGame()
    {
        Debug.LogError("Succesfully quit application");
        Application.Quit();
        
    }


    // Update is called once per frame
    void Update()
    {
       /* if(disinfect.isDisinfected == true)
        {
            isClean = disinfect.isDisinfected;
        }*/
        
    }

    IEnumerator LoadAsync(int levelIndex)
    {
        AsyncOperation operation = SceneManager.LoadSceneAsync(levelIndex);
        loadingScreen.SetActive(true);
        while (!operation.isDone)
        {
            slider.value = operation.progress;
            yield return null;
        }
    }
    void Start()
    {
        //fadescreen.color = Color.black;
        StartCoroutine(FadeIn(1f));
    }

    IEnumerator FadeIn(float seconds)
    {
        float delta = (1f / 60) / seconds;
        for (float i = 1; fadescreen.color.a > 0; i -= delta)
        {
            var tempColor = fadescreen.color;
            tempColor.a = i;
            fadescreen.color = tempColor;
            yield return new WaitForEndOfFrame();
        }
        //for (float i = 0; i <= 1; i += delta)
        //{
        //    fadeMat.SetFloat("_Fade", i);
        //    yield return new WaitForEndOfFrame();
        //}
    }
    IEnumerator FadeOut(float seconds)
    {
        Debug.Log("FADING OUT");
        float delta = (1f / 60) / seconds;
        for (float i = 0; fadescreen.color.a < 1; i += delta)
        {
            Debug.Log(fadescreen.color.a);
            var tempColor = fadescreen.color;
            tempColor.a = i;
            fadescreen.color = tempColor;
            yield return new WaitForEndOfFrame();
        }
        //for (float i = 1; i >= 0; i -= delta)
        //{
        //    fadeMat.SetFloat("_Fade", i);
        //    yield return new WaitForEndOfFrame();
        //}
    }
    IEnumerator DoorTransition(string scene)
    {
        StartCoroutine(FadeOut(1f));
        this.GetComponent<AudioSource>().Play();
        yield return new WaitForSecondsRealtime(2f);
        SceneManager.LoadScene(scene);
    }
}
