using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.XR.Interaction.Toolkit;
using UnityEngine.InputSystem;
using UnityEngine.Events;
using System;

public class TeleportationVisualToggler : MonoBehaviour
{
    public InputActionReference activationButton;

    public UnityEvent onTeleportActivate;
    public UnityEvent onTeleportCancel;

    void Start()
    {
        activationButton.action.performed += TeleportModeActivate;
        activationButton.action.canceled += TeleportModeCancel;
    }

    private void TeleportModeCancel(InputAction.CallbackContext obj)
    {
        Invoke("DeactivateTeleporter", 0.1f);
    }
    void DeactivateTeleporter()
    {
        onTeleportCancel.Invoke();
    }
    private void TeleportModeActivate(InputAction.CallbackContext obj)
    {
        onTeleportActivate.Invoke();
    }
}
