using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class timerscript : MonoBehaviour
{
    public int secondsWait;
    // Start is called before the first frame update
    /*void Start()
    {
        StartCoroutine(TimeBeforeDisappear());
    }*/

    // Update is called once per frame
    IEnumerator TimeBeforeDisappear()
    {
        yield return new WaitForSeconds(secondsWait);
        this.gameObject.SetActive(false);
    }
    
    void Awake()
    {
        StartCoroutine(TimeBeforeDisappear());
    }
}
