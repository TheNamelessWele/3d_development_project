using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SceneTransitionScript : MonoBehaviour
{
    public Image image;

    void Start()
    {
        image.color = Color.black;
        StartCoroutine(FadeIn(5f));
    }

    IEnumerator FadeIn(float seconds)
    {
        float delta = (1f / 60) / seconds;
        for (float i = 1; image.color.a > 0 ; i = i - delta)
        {
            var tempColor = image.color;
            tempColor.a = i;
            image.color = tempColor;
            yield return new WaitForEndOfFrame();
        }
    }
    IEnumerator FadeOut(float seconds)
    {
        float delta = (1f / 60) / seconds;
        for (float i = 0; image.color.a > 1; i = i + delta)
        {
            var tempColor = image.color;
            tempColor.a = i;
            image.color = tempColor;
            yield return new WaitForEndOfFrame();
        }
    }
}
