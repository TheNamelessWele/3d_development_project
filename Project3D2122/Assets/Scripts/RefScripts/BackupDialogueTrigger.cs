using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DialogueTrigger : MonoBehaviour
{
    private Dialogue dialogue = new Dialogue();
    public string npcName = "Z. Eikenhuis";
    public string[] sentences =  { "Hallo, hallo", "Bent u daar eindelijk? ", "Wilt u mijn fles water aangeven? Ik heb het laten vallen." };

    private void Start()
    {
        this.dialogue.name = npcName;
        this.dialogue.sentences = sentences;
    }

    public void TriggerDialogue()
    {
        Debug.LogError("Entering"); //logs if DialogueTrigger has been triggered
        Debug.LogError(dialogue.sentences[0].ToString() + "something something");
        FindObjectOfType<DialogueManager>().StartDialogue(this.dialogue);
        
    }
}
