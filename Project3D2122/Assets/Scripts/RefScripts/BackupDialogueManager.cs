using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.Threading;

public class DialogueManager : MonoBehaviour
{
    public Queue<string> demoSentences;

    public Text nameText;
    public Text dialogueText;

    // Start is called before the first frame update
    void Start()
    {
        demoSentences = new Queue<string>(); 
    }

    public void StartDialogue(Dialogue dialogue)
    {
        
        nameText.text = dialogue.name;

        //demoSentences.Clear();

        foreach(string sentence in dialogue.sentences)
        {
            Debug.LogError(sentence);
            demoSentences.Enqueue(sentence);
            DisplayNextSentence();
        }

        Thread.Sleep(5000);
        DisplayNextSentence();
    }

    public void DisplayNextSentence()
    {
        if(demoSentences.Count == 0)
        {
            FindObjectOfType<Player>().EndDialogue();
            return;
        }
        else
        {
            string sentence = demoSentences.Dequeue();
            Debug.LogError("Started convo");
            dialogueText.text = sentence;
        }
    }
    
}
