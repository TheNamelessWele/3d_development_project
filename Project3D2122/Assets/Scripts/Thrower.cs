using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Thrower : MonoBehaviour
{
    public GameObject heldItem;
    public int ThrowForce = 10000;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    //Function called by animation event
    public void ThrowObject()
    {
        heldItem.transform.parent = null;
        Rigidbody rb = heldItem.GetComponent<Rigidbody>();
        rb.isKinematic = false;
        rb.AddForce(gameObject.transform.forward * ThrowForce);
        Debug.Log("THROWING ITEM");
    }
}
