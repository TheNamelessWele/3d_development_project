using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class BottleRemover : MonoBehaviour
{
    // To give the NPC his medicine
    public TMPro.TextMeshProUGUI npcText;


    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Npc" && other.GetComponent<NPCScript>().aggroLevel == 0)
        {
            npcText.text = "Nou. Bedankt zeg. Ik pak het wel.";
            other.gameObject.SetActive(false);
        }
    }
}
