using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class AudioPlaying : MonoBehaviour
{
    AudioSource audioSource;
    // Start is called before the first frame update
    void Start()
    {
        audioSource = GetComponent<AudioSource>();
        Destroy(audioSource, 48);
    }

    // Update is called once per frame
    void Update()
    {
        if (audioSource == null)
        {
            SceneManager.LoadScene(1);
        }
    }
}
