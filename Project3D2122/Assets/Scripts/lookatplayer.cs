using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class lookatplayer : MonoBehaviour
{
    public Transform target;

    // Update is called once per frame
    void Update()
    {
        Vector3 lookdirection = target.transform.position;
        lookdirection.y = this.transform.position.y;
       transform.LookAt(2 * transform.position - lookdirection); 
    }
}
