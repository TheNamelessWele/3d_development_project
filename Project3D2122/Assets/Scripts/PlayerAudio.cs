using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerAudio : MonoBehaviour
{
    public AudioSource npcSource;
    public AudioSource audioSource;
    public AudioClip audioClip1;
    public AudioClip audioClip2;
    public AudioClip audioClip3;
    public AudioClip audioClip4;
    public AudioClip audioClip5;
    public AudioClip audioClip6;
    public AudioClip audioClip7;
    public AudioClip audioClip8;
    public AudioClip audioClip9;
    public AudioClip audioClip10;
    public AudioClip audioClip11;
    public AudioClip audioClip12;
    public AudioClip audioClip13;
    public AudioClip audioClip14;
    public AudioClip audioClip15;
    public AudioClip audioClip16;
    public AudioClip audioClip17;
    public AudioClip audioClip18;
    public AudioClip audioClip19;
    public AudioClip audioClip20;
    public AudioClip audioClip21;
    public AudioClip audioClip22;
    public AudioClip audioClip23;
    public AudioClip audioClip24;
    public AudioClip audioClip25;
    public AudioClip audioClip26;


    //check if there's a clip already playing
    public void PlayClip1()
    {
        audioSource.clip = audioClip1;
        audioSource.Play();
    }
    public void PlayClip2()
    {
        audioSource.clip = audioClip2;
        audioSource.Play();
    }
    public void PlayClip3()
    {
        audioSource.clip = audioClip3;
        audioSource.Play();
    }
    public void PlayClip4()
    {
        audioSource.clip = audioClip4;
        audioSource.Play();
    }
    public void PlayClip5()
    {
        audioSource.clip = audioClip5;
        audioSource.Play();
    }
    public void PlayClip6()
    {
        audioSource.clip = audioClip6;
        audioSource.Play();
    }
    public void PlayClip7()
    {
        audioSource.clip = audioClip7;
        audioSource.Play();
    }
    public void PlayClip8()
    {
        audioSource.clip = audioClip8;
        audioSource.Play();
    }
    public void PlayClip9()
    {
        audioSource.clip = audioClip9;
        audioSource.Play();
    }
    public void PlayClip10()
    {
        audioSource.clip = audioClip10;
        audioSource.Play();
    }
    public void PlayClip11()
    {
        audioSource.clip = audioClip11;
        audioSource.Play();
    }
    public void PlayClip12()
    {
        audioSource.clip = audioClip12;
        audioSource.Play();
    }
    public void PlayClip13()
    {
        audioSource.clip = audioClip13;
        audioSource.Play();
    }
    public void PlayClip14()
    {
        audioSource.clip = audioClip14;
        audioSource.Play();
    }
    public void PlayClip15()
    {
        audioSource.clip = audioClip15;
        audioSource.Play();
    }
    public void PlayClip16()
    {
        audioSource.clip = audioClip16;
        audioSource.Play();
    }
    public void PlayClip17()
    {
        audioSource.clip = audioClip17;
        audioSource.Play();
    }
    public void PlayClip18()
    {
        audioSource.clip = audioClip18;
        audioSource.Play();
    }
    public void PlayClip19()
    {
        audioSource.clip = audioClip19;
        audioSource.Play();
    }
    public void PlayClip20()
    {
        audioSource.clip = audioClip20;
        audioSource.Play();
    }
    public void PlayClip21()
    {
        audioSource.clip = audioClip21;
        audioSource.Play();
    }
    public void PlayClip22()
    {
        audioSource.clip = audioClip22;
        audioSource.Play();
    }
    public void PlayClip23()
    {
        audioSource.clip = audioClip23;
        audioSource.Play();
    }
    public void PlayClip24()
    {
        audioSource.clip = audioClip24;
        audioSource.Play();
    }
    public void PlayClip25()
    {
        audioSource.clip = audioClip25;
        audioSource.Play();
    }
    public void PlayClip26()
    {
        audioSource.clip = audioClip26;
        audioSource.Play();
    }
}
