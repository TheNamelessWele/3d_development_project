using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.AI;

public class NpcNavigation : MonoBehaviour
{
    public GameObject player;
    public GameObject target;
    public GameObject hand;
    public GameObject bed;

    public float viewRadius = 1;
    public float viewAngle = 60f;
    public bool isRotationDone;

    public AudioSource attackSoundSource;
    public AudioClip[] attackSounds;

    bool isInBed;
    NavMeshAgent agent;
    Animator animator;
    NPCScript npc;
    Coroutine currentBehaviour = null;

    Dictionary<GameObject, float> objectsInView = new Dictionary<GameObject, float>();
    List<GameObject> throwableObjectsInView = new List<GameObject>();

    void Start()
    {
        agent = GetComponent<NavMeshAgent>();
        animator = GetComponent<Animator>();
        npc = GetComponent<NPCScript>();
        //StartCoroutine(Pace());
    }
    void Update()
    {
        animator.SetFloat("Speed", agent.velocity.magnitude);
        animator.SetFloat("Distance", Vector3.Distance(transform.position, target.transform.position));
        animator.SetFloat("DistanceToPlayer", Vector3.Distance(transform.position, player.transform.position));
        //AnalyseEnvironment(0.1f);
    }

    IEnumerator Sleep()
    {
        if (!isInBed)
        {
            agent.SetDestination(bed.transform.position);
            while (agent.remainingDistance < 0.2f)
            {
                yield return null;
            }
            EnterBed();
        }
    }
    IEnumerator Wander()
    {
        while (true)
        {
            agent.SetDestination(GetRandomPosInNav(transform.position, 3));
            while (agent.remainingDistance > 0.1f)
            {
                yield return null;
            }
            yield return new WaitForSeconds(Random.Range(3f, 10f));
        }

    }
    IEnumerator Pace()
    {
        //NavMesh.SamplePosition(transform.position - player.transform.TransformDirection(Vector3.right), out NavMeshHit hit1, 2, -1);
        //NavMesh.SamplePosition(transform.position + player.transform.TransformDirection(Vector3.right), out NavMeshHit hit2, 2, -1);
        //while (true)
        //{
        //    agent.SetDestination(hit1.position);
        //    while (agent.remainingDistance > 0.1f)
        //    {
        //        yield return null;
        //    }
        //    agent.SetDestination(hit2.position);
        //    while (agent.remainingDistance > 0.1f)
        //    {
        //        yield return null;
        //    }
        //}
        yield return null;
    }
    IEnumerator AnalyseEnvironment(float delay)
    {
        while (true)
        {
            FindObjectsInView();
            RemoveOldObjects();
            yield return new WaitForSeconds(delay);
        }
    }
    IEnumerator Attack()
    {
        if ((transform.position - target.transform.position).sqrMagnitude < 0.5)
        {
            animator.SetBool("Attack", true);
        }
        else
        {
            animator.SetBool("Attack", false);
        }
        yield return null;
    }
    IEnumerator Persuit(GameObject target)
    {
        Vector3 navPos = Vector3.Normalize(gameObject.transform.position - target.transform.position);
        agent.SetDestination(target.transform.position + navPos*1.5f);
        Debug.Log("navigating to my target");
        yield return new WaitForSeconds(1); 
        //yield return null;
    }
    public IEnumerator LookAtSmoothly(Transform target, float duration)
    {
        isRotationDone = false;
        Quaternion currentRot = transform.rotation;
        Quaternion newRot = Quaternion.LookRotation(target.position - transform.position, transform.TransformDirection(Vector3.up));

        float counter = 0;
        while (counter < duration)
        {
            counter += Time.deltaTime;
            transform.rotation = Quaternion.Lerp(currentRot, newRot, counter / duration);
            yield return null;
        }
        isRotationDone = true;
    }

    public void FindObjectsInView()
    {
        Collider[] nearbyObjects = Physics.OverlapSphere(transform.position, viewRadius);

        foreach (Collider obj in nearbyObjects)
        {
            Vector3 direction = (obj.transform.position - transform.position).normalized;
            direction.y = 0;

            if (Mathf.Abs(Vector3.Angle(transform.forward, direction)) < viewAngle / 2)
            {
                float distToObj = Vector3.Distance(transform.position, obj.transform.position);

                if (!Physics.Raycast(transform.position, direction, distToObj))
                {
                    if (!objectsInView.ContainsKey(obj.gameObject))
                    {
                        objectsInView.Add(obj.gameObject, Time.time);
                    }
                    else
                    {
                        objectsInView[obj.gameObject] = Time.time;
                    }
                }
            }
        }
    }
    void RemoveOldObjects()
    {
        if (objectsInView.Count > 0)
        {
            var ObjectsToRemove = objectsInView.Where(x => Time.time > x.Value + 2f).ToArray();
            foreach (var item in ObjectsToRemove)
            {
                objectsInView.Remove(item.Key);
            }
        }
    }
    void EnterBed()
    {

    }
    static Vector3 GetRandomPosInNav(Vector3 origin, float radius)
    {
        Vector3 randomDirection = Random.insideUnitSphere * radius;
        randomDirection += origin;

        NavMesh.SamplePosition(randomDirection, out NavMeshHit navMeshHit, radius, -1);
        return navMeshHit.position;
    }
    public void FindNearbyObjects()
    {
        //if i'm already holding an object, make my target the player
        if (GetComponent<Thrower>().heldItem != null)
        {
            target = player;
            return;
        }

        //look for objects i can pick up nearby
        Collider[] nearbyObjects = Physics.OverlapSphere(transform.position, viewRadius, LayerMask.GetMask("Interact"));

        //if there are objects nearby, randomly pick one and go get it
        if (nearbyObjects.Length != 0)
        {
            print("found object(s), persuiting");
            animator.SetBool("HasNearbyObjects", true);
            target = nearbyObjects[Random.Range(0, nearbyObjects.Length)].gameObject;
            //GetComponent<Thrower>().heldItem = target;

            //debug code
            //foreach (Collider item in nearbyObjects)
            //{
            //    print(item.name);
            //}
        }
        else
        {
            print("no objects nearby, making player the target");
            animator.SetBool("HasNearbyObjects", false);
            target = player;
        }
    }

    public void PlayAttackSound()
    {
        attackSoundSource.clip = attackSounds[Random.Range(0, attackSounds.Length)];
        attackSoundSource.Play();
    }
}
