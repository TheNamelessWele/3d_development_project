using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DefenceObj : MonoBehaviour
{
    public GameObject defenseObject;
    public GameObject player;
    public void DefendSelf()
    {
            defenseObject.transform.rotation = new Quaternion(defenseObject.transform.rotation.y, player.gameObject.transform.rotation.x, defenseObject.transform.rotation.z, defenseObject.transform.rotation.w);
    }
}
