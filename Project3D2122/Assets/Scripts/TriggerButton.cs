using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TriggerButton : MonoBehaviour
{
    bool dialogueShows = false;
    void OnTriggerEnter(Collider other)
    {
       
        if(other.tag == "Player")
        {
            //Debug.LogError("Trigger script works");
            FindObjectOfType<Player>().StartingDialogue();
            dialogueShows = true;
        }
    }

    void OnTriggerStay(Collider other)
    {
        if(other.tag == "Player")
        {
            //Debug.LogError("Trigger script works on Stay");
            if (dialogueShows == false)
            {
                FindObjectOfType<Player>().StartingDialogue();
                dialogueShows = true;
            }
            else
            {
                Debug.LogError("Issue in TriggerButton");
            }
        }
        
    }

    void OnTriggerExit(Collider other)
    {
        Debug.LogError("Exited collider");
    }
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
