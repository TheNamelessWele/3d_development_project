using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoundOnImpact : MonoBehaviour
{
    public float minimumVelocity = 1;
    public AudioClip[] sounds;


    private AudioSource player;

    // Start is called before the first frame update
    void Start()
    {
        if (!TryGetComponent<AudioSource>(out player))
        {
            Debug.LogError("no audio source found on the object!");
        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnCollisionEnter(Collision collision)
    {
        //Debug.LogWarning(collision.relativeVelocity.magnitude);
        if (collision.relativeVelocity.magnitude > minimumVelocity)
        {
            if (!player.isPlaying)
            {
                player.clip = sounds[Random.Range(0, sounds.Length)];
                player.volume = collision.relativeVelocity.magnitude / 5;
                player.Play();
            }
        }
    }
}
