using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Disinfect : MonoBehaviour
{
    public bool isDisinfected = false;
    public GameObject disinfect;

    private AudioSource source; //lets the water make noise

    private void Start()
    {
        source = GetComponent<AudioSource>();
    }

    private void OnTriggerEnter(Collider other)
    {
        if(other.tag == "hand" && !isDisinfected)
        {
            isDisinfected = true;
            disinfect.SetActive(true);
        }
    }

    private void OnTriggerStay(Collider other)
    {
        if (other.tag == "hand" && !isDisinfected)
        {
            isDisinfected = true;
            disinfect.SetActive(true);
        }
    }

}
