using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DialogueScript : MonoBehaviour
{   //To do: 
    //Seperate peaceful and aggressive situations 
    //Working with different locations, and some of them have peaceful situations, and some of them have aggro situations
    //Situation ends when clicking on door to leave the room
    //Options in menu:
    //Tags determine which options are shown in the menu
    //Tags on text within the situation tag determine whether the choice was a good or a bad one (or just method "GoodChoice" and "BadChoice"? 


    //situations
    public int aggroSituationNum;
    public int peaceSituationNum;
    public string[] aggroSituations = { "aggro0", "aggro1", "aggro2", "aggro43", "aggro4", "aggro5", "aggro6", "aggro7" };
    public string[] peaceSituations = { "peace0", "peace1", "peace2", "peace3", "peace4", "peace5", "peace6", "peace7" };
    private bool hadAggroSit = false;
    private bool hadPeaceSit = true;
    private bool isPeace = false;

    private int sentenceNumber = 0;
    private int prevAggroLvl = -1;
    private string currentTag = "";
    private NPCScript npcScript;


    public GameObject dialogueBox;
    public DialogueOptions dialogueOptions;
    public GameObject buttonGroup;

    public Text text;

    private List<string> sentences;
    private List<AudioClip> clips;
    public AudioSource source;
    public AudioSource playerSource;
    public AudioClip identifySelf;
    public void StartSituation()
    {
        npcScript = FindObjectOfType<NPCScript>();

            StartDialogue();

    }

    //makes aggro levels go up and down
    public void ButtonGood() { ButtonPress(-1); }
    public void ButtonBad() { ButtonPress(1); }
    public void ButtonNeutral() { ButtonPress(0); }

    private void ButtonPress(int choice)
    {
        npcScript.aggroLevel += choice;
        StartDialogue();
    }

    public void Identify()
    {
        StartCoroutine(playIdentify());
    }
    public void StartDialogue()
    {
        //print npc text
        if (!isPeace)
        {
            int situationNum = aggroSituationNum;
            int aggroLevel = npcScript.aggroLevel;

            //for the animator:
            GetComponent<Animator>().SetInteger("AggroLevel", aggroLevel);

            //for NPC dialogue
            if (aggroLevel == prevAggroLvl) //if the aggroLevel is the same as the previous one (neutral option), increase sentence number
            {
                if(sentenceNumber < 2)
                {
                    sentenceNumber++;
                }
                else
                {
                    sentenceNumber = 0;
                }
            }
            else
            {
                sentenceNumber = 0;
            }
            prevAggroLvl = aggroLevel;

            sentences = dialogueOptions.AggroDialogue(situationNum, aggroLevel);
            clips = dialogueOptions.audioclips;



            StartCoroutine(playAudio());
            
            text.text = sentences[sentenceNumber];
            
            dialogueBox.SetActive(true);

            //for NPC audio
            

            switch (aggroLevel)
            {
                case 0:
                    currentTag = "DEndSit";
                    //add state machine stuff
                    break;
                case 1:
                    currentTag = "DLevel1";
                    //add state machine stuff
                    break;
                case 2:
                    currentTag = "DLevel2";
                    //add state machine stuff
                    break;
                case 3:
                    currentTag = "DLevel3";
                    //add state machine stuff
                    break;
                case 4:
                    currentTag = "DLevel4";
                    //add state machine stuff
                    break;
                case 5:
                    currentTag = "DLevel5";
                    //add state machine stuff
                    break;
                case 6:
                    currentTag = "DLevel6";
                    //add state machine stuff
                    break;
                case 7:
                    currentTag = "DLevel7";
                    //add state machine stuff
                    break;
                default:
                    break;
            }

        }
        //enable player responses, the buttons available depending on the tag which is dependant on the aggro level. 
        foreach (Transform item in buttonGroup.transform)
        {
            if(item.gameObject.tag == currentTag)
            {
                item.gameObject.SetActive(true);
            }
            else item.gameObject.SetActive(false);
        }

    }

    IEnumerator playAudio()
    {
        //print(sentenceNumber);
        source.clip = clips[sentenceNumber];
        if (playerSource.isPlaying)
        {
            yield return new WaitForSeconds(3);
        }
        source.Play();
        yield return new WaitWhile(() => playerSource.isPlaying);
    }

    IEnumerator playIdentify()
    {
        source.clip = identifySelf;
        yield return new WaitForSeconds(3);
        dialogueBox.SetActive(true);
        source.Play();
        yield return new WaitWhile(() => playerSource.isPlaying);
    }


    public void EndDialogue()
    {
        dialogueBox.SetActive(false);
        Debug.LogError("Dialogue Ended");
    }

    //trying to fix this shit, and create an aggrodialogue

}
